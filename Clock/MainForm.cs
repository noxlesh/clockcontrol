﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsDraw
{
    public partial class MainForm : Form
    {
        private const int _clockOffset = 270; // tilt the clock circle to angle
        private float[] sin = new float[360]; // to store sine values for every andgle in a circle
        private float[] cos = new float[360]; // the same for cosine values
        private Font digitFont;
        private bool _transparency; // flag to switch transparency of the clock background

        public MainForm()
        {
            
            for (int i = 0; i < 360; i++)
            {
                sin[i] = (float)Math.Sin(i * Math.PI / 180.0F);
                cos[i] = (float)Math.Cos(i * Math.PI / 180.0F);
            }
            digitFont = new Font("Arial", 12);
            InitializeComponent();
            SetTransparent(_transparency);
            notifyIcon.ContextMenuStrip = contextMenuStrip;
            this.ContextMenuStrip = contextMenuStrip;
        }

        #region An event subscribers
        /// <summary>
        /// The clock draw cycle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            PointF centerPoint = new PointF(ClientSize.Width / 2.0F, ClientSize.Height / 2.0F);
            DrawHourArrow(graphics, centerPoint);
            DrawMinuteArrow(graphics, centerPoint);
            DrawSecondArrow(graphics, centerPoint);
            DrawDotsAndDigits(graphics, centerPoint);
        }

        /// <summary>
        /// Updates the clock every 100 ms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clockTimer_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void toolStripMenuExit_Click(object sender, EventArgs e)
        {
            notifyIcon.Dispose();
            this.Close();
        }

        private void toolStripMenuTransparency_Click(object sender, EventArgs e)
        {
            _transparency = _transparency ? false : true;
            SetTransparent(_transparency);
        }
        #endregion

        #region A clock element drawers
        /// <summary>
        /// Draws the second arrow into the clock
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawSecondArrow(Graphics graphics, PointF center)
        {

            float arrowRadius = 200;
            float tailRadius = 30;
            int arrowAngle = DateTime.Now.Second * 6 + _clockOffset;
            int tailAngle = arrowAngle - 180;
            PointF arrowEnd = new PointF(center.X + arrowRadius * cos[arrowAngle % 360], center.Y + arrowRadius * sin[arrowAngle % 360]);
            PointF tailEnd = new PointF(center.X + tailRadius * cos[tailAngle % 360], center.Y + tailRadius * sin[tailAngle % 360]);
            Pen p = new Pen(Color.Red, 3);
            // main arrow
            graphics.DrawLine(p, center, arrowEnd);
            // arrow tail
            graphics.DrawLine(p, center, tailEnd);
            // center dot
            graphics.FillEllipse(Brushes.Red, center.X - 8.0F, center.Y - 8.0F, 16.0F, 16.0F);
            graphics.FillEllipse(Brushes.Gray, center.X - 3.0F, center.Y - 3.0F, 6.0F, 6.0F);


        }

        /// <summary>
        /// Draws the minute arrow into the clock
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawMinuteArrow(Graphics graphics, PointF center)
        {
            float radius = 190;
            int angle = DateTime.Now.Minute * 6 + _clockOffset;
            PointF endPoint = new PointF(center.X + radius * cos[angle % 360], center.Y + radius * sin[angle % 360]);
            Pen p = new Pen(Color.White, 5);
            graphics.DrawLine(p, center, endPoint);
        }

        /// <summary>
        /// Draws the hour arrow into the clock
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawHourArrow(Graphics graphics, PointF center)
        {
            float radius = 180;
            int angle = DateTime.Now.Hour * 30 + _clockOffset;
            PointF endPoint = new PointF(center.X + radius * cos[angle % 360], center.Y + radius * sin[angle % 360]);
            Pen p = new Pen(Color.White, 8);
            graphics.DrawLine(p, center, endPoint);

        }

        /// <summary>
        /// Graduate clock for seconds, minutes and hours
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawDotsAndDigits(Graphics graphics, PointF center)
        {
            for (int i = 0; i < 60; i++)
            {
                float dotSize = 5.0F;
                float drawRadius = 220;
                float drawDigitRadius = 240;
                int angle = i * 6 + _clockOffset;
                if (i % 5 == 0) // draw Hour digits and big dot every 5 ticks
                {
                    dotSize = 9.0F;
                    string digitStr = i == 0 ? "12" : (i / 5).ToString();
                    Size digitSize = TextRenderer.MeasureText(digitStr, digitFont);
                    PointF digitPt = new PointF(
                        center.X + drawDigitRadius * cos[angle % 360] - digitSize.Width / 2,
                        center.Y + drawDigitRadius * sin[angle % 360] - digitSize.Height / 2);
                    graphics.DrawString(digitStr, digitFont, Brushes.WhiteSmoke, digitPt);
                }
                PointF endPoint = new PointF(
                    center.X + drawRadius * cos[angle % 360] - dotSize / 2,
                    center.Y + drawRadius * sin[angle % 360] - dotSize / 2);
                graphics.FillEllipse(Brushes.Wheat, new RectangleF(endPoint, new SizeF(dotSize, dotSize)));
            }
        }
        #endregion

        /// <summary>
        /// Switches clock window background transparency
        /// </summary>
        /// <param name="tranparency">true to enables transparency, false makes background solid</param>
        private void SetTransparent(bool tranparency)
        {
            if (tranparency)
            {
                this.BackColor = System.Drawing.Color.Black;
                this.TransparencyKey = System.Drawing.Color.Black;
                this.toolStripMenuTransparency.Text = "Make opaque";
            } else
            {
                this.BackColor = Color.Gray;
                this.toolStripMenuTransparency.Text = "Make transparent";
            }
        }
    }
}
