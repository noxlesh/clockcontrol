﻿namespace ClockControlTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clockControl1 = new ClockControl.ClockControl();
            this.SuspendLayout();
            // 
            // clockControl1
            // 
            this.clockControl1.Antialias = true;
            this.clockControl1.DigitsColor = System.Drawing.Color.Black;
            this.clockControl1.HourArrowColor = System.Drawing.Color.Black;
            this.clockControl1.HourDotsColor = System.Drawing.Color.YellowGreen;
            this.clockControl1.Location = new System.Drawing.Point(137, 34);
            this.clockControl1.MinuteArrowColor = System.Drawing.Color.Black;
            this.clockControl1.MinuteDotsColor = System.Drawing.Color.DarkSlateGray;
            this.clockControl1.Name = "clockControl1";
            this.clockControl1.SecondArrowColor = System.Drawing.Color.Brown;
            this.clockControl1.Size = new System.Drawing.Size(474, 474);
            this.clockControl1.TabIndex = 0;
            this.clockControl1.Time_Zone = "(UTC-08:00) Тихоокеанское время (США и Канада)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 550);
            this.Controls.Add(this.clockControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private ClockControl.ClockControl clockControl1;
    }
}

