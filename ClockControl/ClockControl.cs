﻿using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.Linq;

namespace ClockControl
{
    
    public partial class ClockControl : UserControl
    {
        [Browsable(true)]
        [Category("Appearance")]
        public Color DigitsColor { get; set; } = Color.Black;
        [Browsable(true)]
        [Category("Appearance")]
        public Color MinuteDotsColor { get; set; } = Color.Black;
        [Browsable(true)]
        [Category("Appearance")]
        public Color HourDotsColor { get; set; } = Color.Black;
        [Browsable(true)]
        [Category("Appearance")]
        public Color HourArrowColor { get; set; } = Color.Black;
        [Browsable(true)]
        [Category("Appearance")]
        public Color MinuteArrowColor { get; set; } = Color.Black;
        [Browsable(true)]
        [Category("Appearance")]
        public Color SecondArrowColor { get; set; } = Color.Red;
        [Browsable(true)]
        [Category("Appearance")]
        public bool Antialias { get; set; } = true;

        private string _zoneName;


        [TypeConverter(typeof(TimeZoneConverter))]
        [Description("Specified Time Zone")]
        [Category("Parameters")]
        public string Time_Zone
        {
            get => _zoneName;
            set
            {
                _zoneName = value;
                SelectedZone = TimeZoneInfo.GetSystemTimeZones().Where(tz => tz.DisplayName == _zoneName).First();
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TimeZoneInfo SelectedZone { get; private set; } = TimeZoneInfo.Local;

        private const int _clockOffset = 270; // tilt the clock circle to angle of 270 degree
        private float[] sin = new float[360]; // to store sine values for every andgle in a circle
        private float[] cos = new float[360]; // the same for cosine values
        private Font digitFont;
        private PointF cp; // clock center point

        public ClockControl()
        {
            for (int i = 0; i < 360; i++)
            {
                sin[i] = (float)Math.Sin(i * Math.PI / 180.0F);
                cos[i] = (float)Math.Cos(i * Math.PI / 180.0F);
            }
            digitFont = new Font("Arial", 12);
            InitializeComponent();
        }

        #region Event Handlers
        private void ClockControl_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            if(Antialias)
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            cp = new PointF(ClientSize.Width / 2.0F, ClientSize.Height / 2.0F);
            DrawHourArrow(graphics, cp);
            DrawMinuteArrow(graphics, cp);
            DrawSecondArrow(graphics, cp);
            DrawDotsAndDigits(graphics, cp);
        }

        private void clockUpdater_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }
        #endregion

        #region A clock element drawers
        /// <summary>
        /// Draws the second arrow into the clock
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawSecondArrow(Graphics graphics, PointF center)
        {

            float arrowRadius = ClientSize.Width / 2.56F;
            float tailRadius = ClientSize.Width / 17.0F;
            float exDotRadius = ClientSize.Width / 170.0F;
            int arrowAngle = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, SelectedZone).Second * 6 + _clockOffset;
            int tailAngle = arrowAngle - 180;
            PointF arrowEnd = new PointF(center.X + arrowRadius * cos[arrowAngle % 360], center.Y + arrowRadius * sin[arrowAngle % 360]);
            PointF tailEnd = new PointF(center.X + tailRadius * cos[tailAngle % 360], center.Y + tailRadius * sin[tailAngle % 360]);
            Pen p = new Pen(SecondArrowColor, 3);
            // main arrow
            graphics.DrawLine(p, center, arrowEnd);
            // arrow tail
            graphics.DrawLine(p, center, tailEnd);
            // center dot
            graphics.FillEllipse(new SolidBrush(SecondArrowColor), center.X - 8.0F, center.Y - 8.0F, 16.0F, 16.0F);
            graphics.FillEllipse(new SolidBrush(HourDotsColor), center.X - 3.0F, center.Y - 3.0F, 6.0F, 6.0F);


        }

        /// <summary>
        /// Draws the minute arrow into the clock
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawMinuteArrow(Graphics graphics, PointF center)
        {
            float radius = ClientSize.Width / 2.7F;
            int angle = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, SelectedZone).Minute * 6 + _clockOffset;
            PointF endPoint = new PointF(center.X + radius * cos[angle % 360], center.Y + radius * sin[angle % 360]);
            Pen p = new Pen(MinuteArrowColor, 5);
            graphics.DrawLine(p, center, endPoint);
        }

        /// <summary>
        /// Draws the hour arrow into the clock
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawHourArrow(Graphics graphics, PointF center)
        {
            float radius = ClientSize.Width / 2.85F;
            int angle = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, SelectedZone).Hour * 30 + _clockOffset;
            PointF endPoint = new PointF(center.X + radius * cos[angle % 360], center.Y + radius * sin[angle % 360]);
            Pen p = new Pen(HourArrowColor, 8);
            graphics.DrawLine(p, center, endPoint);

        }

        /// <summary>
        /// Graduate clock for seconds, minutes and hours
        /// </summary>
        /// <param name="graphics">Graphics object to which draw</param>
        /// <param name="center">PointF is the center of the clock</param>
        private void DrawDotsAndDigits(Graphics graphics, PointF center)
        {
            for (int i = 0; i < 60; i++)
            {
                Color dotColor = MinuteDotsColor;
                float dotSize = 5.0F;
                float drawRadius = ClientSize.Width / 2.33F;
                float drawDigitRadius = ClientSize.Width / 2.14F;
                int angle = i * 6 + _clockOffset;
                if (i % 5 == 0) // draw Hour digits and big dot every 5 ticks
                {
                    dotSize = 9.0F;
                    dotColor = HourDotsColor;
                    string digitStr = i == 0 ? "12" : (i / 5).ToString();
                    Size digitSize = TextRenderer.MeasureText(digitStr, digitFont);
                    PointF digitPt = new PointF(
                        center.X + drawDigitRadius * cos[angle % 360] - digitSize.Width / 2,
                        center.Y + drawDigitRadius * sin[angle % 360] - digitSize.Height / 2);
                    graphics.DrawString(digitStr, digitFont, new SolidBrush(DigitsColor), digitPt);
                }
                PointF endPoint = new PointF(
                    center.X + drawRadius * cos[angle % 360] - dotSize / 2,
                    center.Y + drawRadius * sin[angle % 360] - dotSize / 2);
                graphics.FillEllipse(new SolidBrush(dotColor), new RectangleF(endPoint, new SizeF(dotSize, dotSize)));
            }
        }
        #endregion

        /// <summary>
        /// Calculates the point on a circle arc at particular angle
        /// </summary>
        /// <param name="center">The center of a circle</param>
        /// <param name="radius">Radius of a circle</param>
        /// <param name="angle">Angle</param>
        private PointF CircleArcPoint(PointF center, float radius, int angle)
        {
            return new PointF(center.X + radius * cos[angle % 360], center.Y + radius * sin[angle % 360]);
        }
    }

    public class TimeZoneConverter : TypeConverter
    {
        public override bool
           GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true; // display drop
        }
        public override bool
           GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true; // drop-down vs combo
        }
        public override StandardValuesCollection
           GetStandardValues(ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(TimeZoneInfo.GetSystemTimeZones());
        }
    }
}
